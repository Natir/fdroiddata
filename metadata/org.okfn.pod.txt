Categories:Office
License:MIT
Web Site:http://okfn.org
Source Code:https://github.com/okfn/product-browser-android
Issue Tracker:https://github.com/okfn/product-browser-android/issues

Auto Name:Product Open Data
Summary:Get product info from barcodes
Description:
No description available
.

Repo Type:git
Repo:https://github.com/okfn/product-browser-android.git

Build:1.00.13,13
    commit=506cfbed0072a8

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.00.13
Current Version Code:13

