Categories:Reading
License:MIT
Web Site:http://opacapp.de
Source Code:https://github.com/raphaelm/opacclient
Issue Tracker:https://github.com/raphaelm/opacclient/issues

Auto Name:Web Opac
Summary:German public libraries
Description:
Client for some German, Austrian and Swiss public libraries that offer online catalogues.
.

Repo Type:git
Repo:https://github.com/raphaelm/opacclient.git

Build:2.0.18,54
    commit=2.0.18
    submodules=yes
    init=rm -rf 3dparty/SlidingMenu/example && \
        (cd 3dparty/HoloEverywhere && \
        git submodule update --init && \
        $$SDK$$/tools/android update project -p contrib/ActionBarSherlock/actionbarsherlock)
    update=.,3dparty/HoloEverywhere/library,3dparty/SlidingMenu/library

Build:2.0.30,66
    commit=2.0.30
    submodules=yes
    init=rm -rf 3dparty/SlidingMenu/example && \
        (cd 3dparty/HoloEverywhere && \
        git submodule update --init && \
        $$SDK$$/tools/android update project -p contrib/ActionBarSherlock/actionbarsherlock)

Build:2.0.31,68
    commit=2.0.31
    submodules=yes
    prebuild=rm -rf 3dparty/SlidingMenu/example

Build:2.0.32,69
    commit=2.0.32
    submodules=yes
    prebuild=rm -rf 3dparty/SlidingMenu/example

Build:2.1.0,70
    commit=2.1.0
    submodules=yes
    prebuild=rm -rf 3dparty/SlidingMenu/example

Build:3.0.1,74
    commit=3.0.1
    submodules=yes

Build:3.0.3,76
    commit=3.0.3
    submodules=yes

Build:3.0.4,77
    commit=3.0.4
    submodules=yes

Build:3.0.5,79
    commit=3.0.5
    submodules=yes

Build:3.0.7,81
    commit=3.0.7
    submodules=yes

Build:3.0.8,82
    commit=3.0.8
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3
    rm=libs/jsoup-*,libs/acra-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ 

Maintainer Notes:
"Auto Update Mode:Version %v" cannot be set due to included jars. Currently
missing are cwac-endless and "adaper-1.0.1.jar".
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:3.0.11
Current Version Code:85

